function consultar() {
    var cep = document.getElementById('cep').value;

    var url = "https://viacep.com.br/ws/" + cep + "/json";

    loadingElement('btnConsultar', 'Consultando...')

    setTimeout(()=> {
        MobileUI.ajax.get(url).end(retornoAPI);
    }, 3000)
}

function retornoAPI(error, res) {
    if (error) return;

    var ret = res.body;
    var stringRetorno = '';

    stringRetorno += ret['bairro'] + '<br>';
    stringRetorno += ret['cep'] + '<br>';
    stringRetorno += ret['complemento'] + '<br>';
    stringRetorno += ret['logradouro'] + '<br>';
    stringRetorno += ret['localidade'] + '<br>';
    stringRetorno += ret['uf'] + '<br>';
    stringRetorno += ret['ibge'] + '<br>';

    document.getElementById('retorno').innerHTML = stringRetorno;
    closeLoading('btnConsultar');
}